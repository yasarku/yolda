package com.example.yolda.controller;

import com.example.yolda.dto.CsvRequest;
import com.example.yolda.dto.Response;
import com.example.yolda.dto.Result;
import com.example.yolda.service.CalculateCostService;
import com.example.yolda.service.CsvRangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/range")
public class RangeController {

    @Autowired
    CalculateCostService calculateCostService;

    @Autowired
    CsvRangeService csvRangeService;

    @PostMapping(value = "/updateRangeParameter")
    public Response updateRangeParameters(@RequestBody CsvRequest request) {
        csvRangeService.defineCsv(request.getCsv());
        return new Response(OK, new Result());
    }

    @GetMapping(value = "/cityCost/{cityName}/{distance}")
    public Response getCityCost(@PathVariable String cityName, @PathVariable Double distance) {
        return new Response(OK, new Result(calculateCostService.rangeCost(cityName, distance)));
    }
}
