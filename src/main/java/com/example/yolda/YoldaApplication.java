package com.example.yolda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoldaApplication {

	public static void main(String[] args) {
		SpringApplication.run(YoldaApplication.class, args);
	}

}
