package com.example.yolda.service;

public interface CalculateCostService {
    public Double rangeCost(String cityName, Double distance);
}
