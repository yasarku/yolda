package com.example.yolda.service.Impl;

import com.example.yolda.dto.CityRange;
import com.example.yolda.dto.Range;
import com.example.yolda.dto.RangeSingleton;
import com.example.yolda.service.CsvRangeService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class CsvRangeServiceImpl implements CsvRangeService {

    @Override
    public void defineCsv(String csv) {
        List<String> lines = Arrays.asList(csv.split("\n"));
        List<Range> rangeList = generateHeaderRangeList(lines.get(0));
        lines = lines.subList(1,lines.size());
        HashMap<String, CityRange> cityRangeHashMap = new HashMap<>();
        for(String line : lines){
            List<String> parameters = Arrays.asList(line.split(";"));
            CityRange cityRange = new CityRange();
            cityRange.setCityName(parameters.get(0));
            cityRange.setDayCost(Integer.parseInt(parameters.get(1)));
            cityRange.setRangeList(generateRangeList(parameters.subList(2,parameters.size()), rangeList));
            cityRangeHashMap.put(cityRange.getCityName(), cityRange);
        }
        RangeSingleton.getInstance().setRangeMap(cityRangeHashMap);
    }

    private List<Range> generateRangeList(List<String> parameters, List<Range> rangeList) {
        List<Range> ranges = new ArrayList<>();
        for(int i = 0; i < rangeList.size(); i++){
            Range range = new Range();
            range.setRangeStart(rangeList.get(i).getRangeStart());
            range.setRangeEnd(rangeList.get(i).getRangeEnd());
            range.setRangeCost(Double.parseDouble(parameters.get(i)));
            ranges.add(range);
        }
        return ranges;
    }

    private List<Range> generateHeaderRangeList(String s) {
        List<String> rangeParameters = Arrays.asList(s.split(";"));
        rangeParameters = rangeParameters.subList(2,rangeParameters.size());
        List<Range> rangeList = new ArrayList<>();
        for(String param : rangeParameters){
            List<String> parameters = Arrays.asList(param.split("-"));
            Range range = new Range();
            range.setRangeStart(Double.parseDouble(parameters.get(0)));
            range.setRangeEnd(Double.parseDouble(parameters.get(1)));
            rangeList.add(range);
        }
        return rangeList;
    }
}
