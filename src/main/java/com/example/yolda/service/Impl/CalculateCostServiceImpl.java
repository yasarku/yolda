package com.example.yolda.service.Impl;

import com.example.yolda.dto.CityRange;
import com.example.yolda.dto.Range;
import com.example.yolda.dto.RangeSingleton;
import com.example.yolda.service.CalculateCostService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

@Service
public class CalculateCostServiceImpl implements CalculateCostService {

    @Override
    public Double rangeCost(String cityName, Double distance) {
        HashMap<String, CityRange> cityRangeHashMap = RangeSingleton.getInstance().getRangeMap();
        CityRange cityRange = cityRangeHashMap.get(cityName);
        if(Objects.isNull(cityRange))
            return null;
        Optional<Range> result = cityRange.getRangeList().stream().filter(range ->
            (range.getRangeStart() <= distance && range.getRangeEnd() >= distance)
        ).findFirst();
        return result.isPresent() ? result.get().getRangeCost() : null;
    }
}
