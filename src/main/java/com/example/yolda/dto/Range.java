package com.example.yolda.dto;

import lombok.Data;

@Data
public class Range {
    private Double rangeStart;
    private Double rangeEnd;
    private Double rangeCost;
}
