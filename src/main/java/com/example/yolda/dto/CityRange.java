package com.example.yolda.dto;

import lombok.Data;

import java.util.List;

@Data
public class CityRange {
    private String cityName;
    private Integer dayCost;
    private List<Range> rangeList;
}
