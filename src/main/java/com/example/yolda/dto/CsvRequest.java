package com.example.yolda.dto;

import lombok.Data;

@Data
public class CsvRequest {
    String csv;
}
