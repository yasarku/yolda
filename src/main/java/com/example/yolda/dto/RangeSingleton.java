package com.example.yolda.dto;

import java.util.HashMap;

public class RangeSingleton {
    private static RangeSingleton rangeSingletonInstance;
    public HashMap<String, CityRange> cityRangeHashMap = new HashMap<String, CityRange>();
    public static RangeSingleton getInstance() {
        if (rangeSingletonInstance == null) {
            {
                rangeSingletonInstance = new RangeSingleton();
            }
        }
        return rangeSingletonInstance;
    }

    public void setRangeMap(HashMap<String, CityRange> cityRangeHashMap)
    {
        this.cityRangeHashMap = cityRangeHashMap;
    }

    public HashMap<String, CityRange> getRangeMap()
    {
        return this.cityRangeHashMap;
    }
}
